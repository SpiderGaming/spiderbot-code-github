[![Total alerts](https://img.shields.io/lgtm/alerts/g/SpiderGamin/SpiderBot-Code.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/SpiderGamin/SpiderBot-Code/alerts/)
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/g/SpiderGamin/SpiderBot-Code.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/SpiderGamin/SpiderBot-Code/context:javascript)
![Discord](https://img.shields.io/discord/728316562163105925?color=blue&label=Discord&logo=%7B%7Bl&logoColor=blue)
# SpiderBot
Spiderbot, Made to do everything, free, forever. (Of course the bot does nothing right now, I'm developing it)  
SpiderBot brings moderation, leveling, games, and much more!  
The bot is still being developed, and has a long way till It's done.

Original plan: Replace all the bots on my Discord server.  
New plan: Replace all the bots on Discord (just kidding, but it helps the slogan ok)

This bot is under heavy development and the code is **A little** messy right now.

[Invite the bot to your server]() (not added yet as the bot is not ready for this)  
[SpiderBot help and testing Discord server](https://discord.gg/6kFYJAP)

## Planned Features
- Swear filter
- Fully customizable guild and user config
- Moderation
- Games, including economy
- Leveling
- Other fun stuff
- And much more!

### Basic Usage
The default prefix is `&`  
The bots commands and their usage can be found here <a href="">documentation</a> (Coming soon)

## Links
- SpiderBot Invite (Not ready)
- [Trello board](https://trello.com/b/5VGBZZ64/spiderbot)
- [SpiderBot Support Discord](https://discord.gg/6kFYJAP)
- SpiderBot Homepage (Not done)


## Run the bot yourself
This is not recommended as you could just add the official bot to your server.  

Install nodejs version `12.0.0` or newer.  
Then navigate to the clones directory in the terminal, run `npm install` to install the needed npm packages.  
Create a file named `.env` in the bots main directory and add the following; `TOKEN=YOURBOTSTOKEN`, replacing YOURBOTSTOKEN with the token you got in the developer portal.  
Rename the file `database.example.sqlite` in the config folder to `database.sqlite`  
To start the bot, run `npm start` or `node index.js`

## Resources Used
- [Discord.js Guide](https://discordjs.guide/)  
- [Discord.js Documentation](https://discord.js.org/?source=post_page---------------------------#/docs/main/stable/general/welcome)  
- [Discord.js discord server](https://discord.gg/bRCvFy9)

## Credits
- [SpiderGamin](https://github.com/SpiderGamin) - Bot Creator and Owner  
- [AbacabaTheAbacus](https://github.com/AbacabaTheAbacus) - Helped with some of the code
- The people of my discord server, who had to put up with the bots annoying spam issues*


### Notes
_I'm still trying to find a good, free, hosting option for this bot (thats not using my raspberry pi)_  
Figure out how to handle command arguments
